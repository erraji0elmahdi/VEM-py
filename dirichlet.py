import numpy as np
from sympy import symbols, lambdify
from VEM import Mesh

# Step 1: Define the exact solution using SymPy
x, y = symbols('x y')
exact_solution_expr = x**3/(y+1) + y**2  # You can change this expression as needed
ndof = 320
# Create a Mesh object
mesh = Mesh(f'./Mesh/Node{ndof}.dat', f'./Mesh/Element{ndof}.dat')
vertices = mesh.vertices
laplacian_exact_solution = exact_solution_expr - exact_solution_expr.diff(x, x) - exact_solution_expr.diff(y, y)
rhs_func = lambdify((x, y), laplacian_exact_solution, 'numpy')
exact_solution_func = lambdify((x, y), exact_solution_expr, 'numpy')
exact_u = exact_solution_func(vertices[:, 0], vertices[:, 1])

# Evaluate RHS at mesh vertices

rhs_values = rhs_func(vertices[:, 0], vertices[:, 1])

u_numerical = np.zeros_like(rhs_values)

RHS = mesh.assemble_RHS1(rhs_values).reshape(rhs_values.shape[0],)
#RHS = mesh.M@rhs_values
RHS_t = np.zeros_like(RHS)
RHS_t[np.ix_(mesh.border)] = exact_u[np.ix_(mesh.border)]

RHS = RHS - mesh.K @ RHS_t

# Step 3: Solve the system numerically using the computed RHS
u_numerical[mesh.internal] = np.linalg.solve(mesh.M[np.ix_(mesh.internal, mesh.internal)] + mesh.K[np.ix_(mesh.internal, mesh.internal)], RHS[mesh.internal])

u_numerical[mesh.border] = exact_u[mesh.border]

# Step 4: Compute the L^2 error
# Define a function for the exact solution
exact_solution_func = lambdify((x, y), exact_solution_expr, 'numpy')


# Compute the L^2 error using the method provided by the mesh object (assuming it exists)
error = mesh.L2error(u_numerical, exact_u)
print("L^2 Error:", error)
linf_norm = np.max(np.abs(u_numerical - exact_u))
print("L^infinity Norm:", linf_norm)

# Plot and output the results
#mesh.plot(u_numerical-exact_u)
mesh.write_vtk(exact_u, 'output1.vtk')