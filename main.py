import numpy as np
from sympy import symbols, lambdify
from VEM import Mesh

# Step 1: Define the exact solution using SymPy
x, y = symbols('x y')
exact_solution_expr = x**2*(1-x)**2*y**2*(1-y)**2   # You can change this expression as needed
ndof = 320
# Create a Mesh object
mesh = Mesh(f'./Mesh/Node{ndof}.dat', f'./Mesh/Element{ndof}.dat')
alpha = 1

# Step 2: Compute the RHS corresponding to the exact solution
# For simplicity, assuming a Laplace equation, the RHS is the negative Laplacian of the exact solution
laplacian_exact_solution = exact_solution_expr - alpha*exact_solution_expr.diff(x, x) - alpha*exact_solution_expr.diff(y, y)
rhs_func = lambdify((x, y), laplacian_exact_solution, 'numpy')

# Evaluate RHS at mesh vertices
vertices = mesh.vertices
rhs_values = rhs_func(vertices[:, 0], vertices[:, 1])
RHS = mesh.assemble_RHS1(rhs_values)

# Step 3: Solve the system numerically using the computed RHS
u_numerical = np.linalg.solve(mesh.M + mesh.K, mesh.M@rhs_values)
#u_numerical = np.linalg.solve(mesh.M + alpha*mesh.K, RHS)

# Step 4: Compute the L^2 error
# Define a function for the exact solution
exact_solution_func = lambdify((x, y), exact_solution_expr, 'numpy')
exact_u = exact_solution_func(vertices[:, 0], vertices[:, 1])

# Compute the L^2 error using the method provided by the mesh object (assuming it exists)
error = mesh.L2error(u_numerical.reshape(len(u_numerical),1), exact_u.reshape(len(u_numerical),1))
print("L^2 Error:", error)
linf_norm = np.max(np.abs(u_numerical.reshape(len(u_numerical),1) - exact_u.reshape(len(u_numerical),1)))
print("L^infinity Norm:", linf_norm)

# Plot and output the results
#mesh.plot(u_numerical)
mesh.write_vtk(u_numerical.reshape(len(u_numerical),), 'appsol.vtk')
mesh.write_vtk(exact_u.reshape(len(u_numerical),), 'excsol.vtk')
mesh.write_vtk(exact_u.reshape(len(u_numerical),) - u_numerical.reshape(len(u_numerical),), 'error.vtk')