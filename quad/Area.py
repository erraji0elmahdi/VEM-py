import numpy as np

def Area(vertices):
    x, y = vertices[:, 0], vertices[:, 1]
    n = len(x) - 1
    r = 0
    for i in range(n):
        r += (x[i] * y[i + 1]) - (x[i + 1] * y[i])
    r += (x[-1] * y[0]) - (x[0] * y[-1])
    r = abs(r) / 2
    return r