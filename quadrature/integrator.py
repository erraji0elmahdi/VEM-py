def quadrature_points_weights(order):
    # Dictionary containing the points and weights for different orders
    rules = {
        1: {
            'points': [(1/3, 1/3)],
            'weights': [1/2]
        },
        2: {
            'points': [(1/2, 0), (0, 1/2), (1/2, 1/2)],
            'weights': [1/6, 1/6, 1/6]
        },
        3: {
            'points': [(1/3, 1/3),
                       (0.6, 0.2), (0.2, 0.6), (0.2, 0.2)],
            'weights': [-27/96, 25/96, 25/96, 25/96]
        },
        4: {
            'points': [(1/3, 1/3),
                       (0.79742699, 0.10128651), (0.10128651, 0.79742699), (0.10128651, 0.10128651),
                       (0.05971587, 0.47014206), (0.47014206, 0.05971587), (0.47014206, 0.47014206)],
            'weights': [0.225, 0.12593918, 0.12593918, 0.12593918, 0.13239415, 0.13239415, 0.13239415]
        },
        5: {
            'points': [(1/3, 1/3),
                       (6/21, 6/21), (6/21, 9/21), (9/21, 6/21),
                       (4/21, 4/21), (4/21, 13/21), (13/21, 4/21)],
            'weights': [9/80, 31/240, 31/240, 31/240, 31/240, 31/240, 31/240]
        }
        # Add more rules as needed
    }
    
    if order in rules:
        return rules[order]['points'], rules[order]['weights']
    else:
        raise ValueError(f"No quadrature rule defined for order {order}")

def integrate_over_triangle(func, order):
    points, weights = quadrature_points_weights(order)
    
    result = 0
    for (a, b), w in zip(points, weights):
        x = a
        y = b
        result += w * func(x, y)
    
    return result

# Example usage
def example_func(x, y):
    return x

# Integrate the function using a specified order of accuracy
integral_result = integrate_over_triangle(example_func, 2)
print("Integral of f(x, y) = x^2 + y^2 over the triangle with order 4:", integral_result)
