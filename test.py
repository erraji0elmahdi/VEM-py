import unittest
import numpy as np
from locals import Local, grad_m, m
from geometry import normal, centroid, integrator

d=np.sqrt(2)
vertices1 = np.array([[0., 0.], [1., 0.], [1., 1.], [0., 1.]])
normals1 = np.array([[0., -1.], [1., -0.], [0., 1.], [-1., 0.]])
vertices2 = np.array([[0.,0.], [3.,0.], [3.,2.], [3./2,4.], [0.,4.]])

B1 = 1/4.*np.array([
    [1, 1, 1, 1],
    [-d, d, d, -d],
    [-d, -d, d, d]
])

D1 = (1/4.) * np.array([
            [4, -d, -d],
            [4, d, -d],
            [4, d, d],
            [4, -d, d]
])

G1 = (1/2.) * np.array([
            [2, 0, 0],
            [0, 1, 0],
            [0, 0, 1]
        ])

B2 = (1/20) * np.array([

    [4, 4, 4, 4, 4],

    [-8, 4, 8, 4, -8],

    [-6, -6, 3, 6, 3]

])



D2 = (1/1470) * np.array([
    [1470, -399, -532],
    [1470, 483, -532],
    [1470, 483, 56],
    [1470, 42, 644],
    [1470, -399, 644]
])



G2 = (1/1050) * np.array([
    [1050, 30, 40],
    [0, 441, 0],
    [0, 0, 441]
])


local1 = Local(vertices1)
local2 = Local(vertices2)

grad_m1 = grad_m(np.array([0.5,0.5]), d)

tri = np.array([[0,0],[1,0],[0,1]])



class TestGeom(unittest.TestCase):
    def test_normals(self):
        np.testing.assert_array_almost_equal(normal(vertices1), normals1, decimal=6)

    def test_monomial(self):
        np.testing.assert_array_almost_equal(grad_m1(np.array([1.,0]),0), np.array([0.,0.]) , decimal=6)
    def test_monomial1(self):
        np.testing.assert_array_almost_equal(grad_m1(np.array([1.,0]),1), np.array([1/d,0.]) , decimal=6)
    def test_monomial12(self):
        np.testing.assert_array_almost_equal(grad_m1(np.array([1.,0]),2), np.array([0.,1/d]) , decimal=6)
    def test_intergator(self):
        np.testing.assert_array_almost_equal(integrator(tri, lambda x : x[0] + x[1]), 1/3., decimal=6)


class TestLoc(unittest.TestCase):
    
    def test_matrix_B(self):
        np.testing.assert_array_almost_equal(local1.get_B(), B1, decimal=6)
    
    def test_matrix_D(self):
        np.testing.assert_array_almost_equal(local1.get_D(), D1, decimal=6)

    def test_matrix_G(self):
        np.testing.assert_array_almost_equal(local1.get_G(), G1, decimal=6)

    def test_matrix_B2(self):
        np.testing.assert_array_almost_equal(local2.get_B(), B2, decimal=6)

    def test_matrix_D2(self):
        np.testing.assert_array_almost_equal(local2.get_D(), D2, decimal=6)

    def test_matrix_G2(self):
        np.testing.assert_array_almost_equal(local2.get_G(), G2, decimal=6)

# This runs the test suite
unittest.main(argv=[''], exit=True)
