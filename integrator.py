import numpy as np
from scipy.integrate import dblquad

def triangle_area(vertices):
    (x1, y1), (x2, y2), (x3, y3) = vertices
    return abs(x1 * (y2 - y3) + x2 * (y3 - y1) + x3 * (y1 - y2)) / 2

class Enumeration:
    def __init__(self, k):
        self.k = k
        self.ij = []
        for i in range(k + 1):
            for j in range(k + 1 - i):
                self.ij.append([j, i])

    def __call__(self, index):
        return self.ij[index] if 0 <= index < len(self.ij) else None

class IntegralMatrix:
    def __init__(self, k):
        self.k = k

    def compute_integral(self, vertices, i, j, x_G, y_G, h):
        x1, y1 = vertices[0]
        x2, y2 = vertices[1]
        x3, y3 = vertices[2]

        # Transformation from reference triangle (0,0)-(1,0)-(0,1) to actual triangle vertices
        def transform(u, v):
            x = x1 + (x2 - x1) * u + (x3 - x1) * v
            y = y1 + (y2 - y1) * u + (y3 - y1) * v
            return (x, y)

        # The integrand function
        def integrand(u, v):
            x, y = transform(u, v)
            return ((x - x_G) / h)**i * ((y - y_G) / h)**j

        # Jacobian of the transformation from the reference triangle to the actual triangle
        jacobian = abs((x2 - x1) * (y3 - y1) - (x3 - x1) * (y2 - y1))

        # Perform the double integration
        result, _ = dblquad(integrand, 0, 1, lambda u: 0, lambda u: 1-u, epsabs=1.49e-12, epsrel=1.49e-12)
        return result * jacobian

def function(vertices, x_G, y_G, h, n=3):
    integral_matrix = IntegralMatrix(n - 1)
    e = Enumeration(1)
    M = np.zeros((n, n))
    for i in range(n):
        for j in range(n):
            M[i, j] = integral_matrix.compute_integral(vertices, e(i)[0] + e(j)[0], e(i)[1] + e(j)[1], x_G, y_G, h)
    return M

def getH(polygon, x_G, y_G, h, n=3):
    M_total = np.zeros((n, n))
    num_vertices = len(polygon)
    base_vertex = polygon[0]
    
    for i in range(1, num_vertices - 1):
        triangle = [base_vertex, polygon[i], polygon[i + 1]]
        M_total += function(triangle, x_G, y_G, h, n)
    
    return M_total

