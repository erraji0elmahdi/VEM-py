import sympy as sp

# Define the variables
u, v = sp.symbols('u v')

# Define the vertices of the triangle
x1, y1 = 0, 0
x2, y2 = 1, 0
x3, y3 = 0, 1

# Define the transformation
x = x1 + (x2 - x1) * u + (x3 - x1) * v
y = y1 + (y2 - y1) * u + (y3 - y1) * v

# Define the function to integrate
f = x**2 + y**2

# Calculate the Jacobian determinant
J = sp.Abs((x2 - x1) * (y3 - y1) - (x3 - x1) * (y2 - y1))

# Set up the integral
integral = sp.integrate(sp.integrate(f * J, (v, 0, 1 - u)), (u, 0, 1))

# Display the result
print(integral)