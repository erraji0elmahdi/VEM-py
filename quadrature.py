import numpy as np
from scipy.special import roots_legendre

def T1(x,y, tri):
    x1, y1 = tri[0]
    x2, y2 = tri[1]
    x3, y3 = tri[2]
    u = (x2-x1)*x + (x3-x1)*y + x1
    v = (y2-y1)*x + (y3-y1)*y + y1
    jac = abs((x2 - x1) * (y3 - y1) - (x3 - x1) * (y2 - y1))
    return u, v, jac

class quadrature:
    def __init__(self, vertices = None, order = 2):
        self.vertices = vertices
        self.order = order
        self.points, self.weights = roots_legendre(order)
        self.triangles = []
        base = self.vertices[0]
        for i in range(1,len(self.vertices)-1):
            self.triangles.append([base, self.vertices[i], self.vertices[i+1]])

    def int_tri(self, func, tri):
        res = 0
        n = len(self.points)
        for i in range(n):
            for j in range(n):
                x = self.points[i]
                y = self.points[j]
                X=(x+1)/2
                Y=(y+1)*(1-(x+1)/2)/2
                u, v, jac = T1(X, Y, tri)
                f = func(u,v)*(1-(x+1)/2)
                res += self.weights[i] * self.weights[j] * f * jac/4
        return res
    
    def __call__(self, func):
        res = 0
        for tri in self.triangles:
            res += self.int_tri(func, tri)
        return res

def func(x, y):
    return x**2 + y # Simple function that's easy to integrate analytically

# Define vertices of a simple triangle for easy analytical integration
vertices = np.array([[0,0], [0.5,0], [1,0], [1,1], [0.5,1], [0,1]])  # Triangle in the first quadrant

quad = quadrature(vertices=vertices, order=2)

# Implementing a corrected version of int_tri and __callable__
result = quad(func)  # This will compute the integral over the triangle

print("Computed integral:", result)