import numpy as np

def centroid(vertices):
    vertices = np.array(vertices)
    rolled_vertices = np.roll(vertices, -1, axis=0)
    common_term = vertices[:, 0] * rolled_vertices[:, 1] - rolled_vertices[:, 0] * vertices[:, 1]
    
    A = 0.5 * np.sum(common_term)
    C_x = (1 / (6 * A)) * np.sum((vertices[:, 0] + rolled_vertices[:, 0]) * common_term)
    C_y = (1 / (6 * A)) * np.sum((vertices[:, 1] + rolled_vertices[:, 1]) * common_term)
    
    return C_x, C_y

def normal(vertices):
    # Ensure there are at least three vertices to form a polygon
    if vertices.shape[0] < 2:
        raise ValueError("At least three vertices are required")
    
    # Compute the centroid of the polygon
    centroid = np.mean(vertices, axis=0)
    
    # Calculate edge vectors
    svertices = np.roll(vertices, -1, axis=0)
    edges = svertices - vertices
    
    # Initialize normals array
    normals = np.zeros_like(edges)
    
    # Loop over edges to compute normals
    for i, (dx, dy) in enumerate(edges):
        # Normalize the normal vector
        normals[i] = np.array([-dy, dx]) / np.linalg.norm(np.array([-dy, dx]))

    for i in range(len(normals)):
        if np.dot(normals[i], (vertices[i] - centroid)) < 0:
            normals[i] = -normals[i]
    
    
    return normals

def centroid(vertices):
    vertices = np.array(vertices)
    rolled_vertices = np.roll(vertices, -1, axis=0)
    common_term = vertices[:, 0] * rolled_vertices[:, 1] - rolled_vertices[:, 0] * vertices[:, 1]
    
    A = 0.5 * np.sum(common_term)
    C_x = (1 / (6 * A)) * np.sum((vertices[:, 0] + rolled_vertices[:, 0]) * common_term)
    C_y = (1 / (6 * A)) * np.sum((vertices[:, 1] + rolled_vertices[:, 1]) * common_term)
    
    return C_x, C_y

def triangle_area(vertices):
    # Extracting x and y coordinates
    x = vertices[:, 0]
    y = vertices[:, 1]
    # Applying the formula
    area = 0.5 * np.abs(x[0]*(y[1] - y[2]) + x[1]*(y[2] - y[0]) + x[2]*(y[0] - y[1]))
    return area

def integrator(x, poly):
    #area of the polygon
    c=centroid(x)
    area=triangle_area(x)
    p=poly(c)

    return area*p
