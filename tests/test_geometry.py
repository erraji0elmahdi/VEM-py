from sympy import symbols, integrate, Matrix
import numpy as np

# Define the variables
x, y, x_G, y_G = symbols('x y x_G y_G')
imax, jmax = symbols('imax jmax', integer=True)


def enumerate_ij(alpha):
    ij = []
    for i in range(alpha+1):
        for j in range(alpha + 1 - i):
            ij.append([j, i])
    return ij

# Define the integral function
def compute_integral(i, j):
    # Expression inside the integral
    expr = (x - x_G)**i * (y - y_G)**j
    # Compute the double integral
    integral_result = integrate(integrate(expr, (y, 0, 1 - x)), (x, 0, 1))
    return integral_result
  
# Assuming values for imax and jmax
imax_val = 2  # Example maximum i value
jmax_val = 2  # Example maximum j value

# Create a matrix to hold the results
result_matrix = Matrix(imax_val+1, jmax_val+1, lambda i, j: compute_integral(i, j))

def compute_H(alpha,x_G, y_G):
    ij = enumerate_ij(alpha)
    res = np.zeros((alpha+1, alpha+1))
    for i in len(ij):
        for j in len(ij):
            res[i, j] = compute_integral(i[0], j[0])
# Substitute x_G and y_G with specific values
x_G_val = 0.3  # Example value for x_G
y_G_val = 0.3  # Example value for y_G

def compute_H(alpha,x_G, y_G):
    ij = enumerate_ij(alpha)
    result_matrix_sub = result_matrix.subs({x_G: x_G, y_G: y_G})
    res = np.zeros((len(ij), len(ij)))
    for i in range(len(ij)):
        for j in range(len(ij)):
            res[i, j] = result_matrix_sub[ij[i][0]+ij[j][0], ij[i][1]+ij[j][1]]

# Convert to a numerical numpy array
M=compute_H(1,0.3,0.3)

# Display the numerical results
print(M)