import numpy as np
from locals import Local
from collections import defaultdict
import matplotlib.pyplot as plt
import logging
import os
from matplotlib.collections import PolyCollection


from scipy.interpolate import LinearNDInterpolator

logging.basicConfig(level=logging.DEBUG)

assert os.path.exists('./Mesh/Node80.dat'), "Node file does not exist at specified path"
assert os.path.exists('./Mesh/Element80.dat'), "Element file does not exist at specified path"


class Mesh:
    def __init__(self, vfile, efile):
        self.vertices = np.array([])  # Initialize an empty NumPy array for vertices
        self.connections = []  # Connections can remain as a list of tuples
        self.vfile = vfile
        self.efile = efile
        self.load_vertices()  # Load vertices immediately upon initialization
        self.load_connections()  # Load connections immediately upon initialization
        self.border = self.getborder()
        self.internal = self.find_internal_indices()
        self.K = self.Stiff()
        self.M = self.Mass()

    def find_internal_indices(self):
        # Set of all indices
        self.getborder()
        all_indices = set(range(len(self.vertices)))
        # Remove boundary indices from all indices to get internal indices
        internal = list(all_indices - set(self.border))
        
        return internal

    
    def getborder(self):
        edge_dict = defaultdict(int)
        connections = self.connections

        for conn in connections:
            # Ensure we're dealing with an unordered pair of vertices for the edges
            for i in range(len(conn)):
                # Create an edge tuple, sorted to avoid directionality
                edge = tuple(sorted((conn[i], conn[(i + 1) % len(conn)])))
                edge_dict[edge] += 1

        # Step 2: Identify boundary edges
        boundary_edges = {edge for edge, count in edge_dict.items() if count == 1}

        # Step 3: Extract the vertices that are part of boundary edges
        boundary_vertices = set()
        for edge in boundary_edges:
            boundary_vertices.update(edge)

        # boundary_vertices now contains the indices of the vertices on the boundary
        return sorted(boundary_vertices)

    def load_vertices(self):
        """
        Load vertex coordinates from a file and store them in a NumPy array.
        Each line in the file represents a vertex and contains coordinates.
        """
        temp_vertices = []  # Temporary list to store vertex data
        with open(self.vfile, 'r') as file:
            for line in file:
                parts = line.split()  # Assuming space-separated values
                if len(parts) == 2:  # Ensure there are two coordinates (x, y)
                    coordinates = list(map(float, parts))  # Convert string to float
                    temp_vertices.append(coordinates)
        self.vertices = np.array(temp_vertices)  # Convert list to NumPy array

    def load_connections(self):
        """
        Load connections between vertices from a file.
        Each line in the file represents a connection and contains two indices,
        referring to vertices in the NumPy array self.vertices.
        """
        with open(self.efile, 'r') as file:
            for line in file:
                parts = line.split()
                ele = list(map(int, parts))
                self.connections.append([x - 1 for x in ele])

    def display(self):
        """
        Display the vertices and connections of the mesh using NumPy array format.
        """
        print("Vertices:")
        for i, (x, y) in enumerate(self.vertices):
            print(f"{i}: ({x}, {y})")
        print("\nConnections:")
        for con in self.connections:
            print(con)

    def Stiff(self):
        n=self.vertices.shape[0]
        K = np.zeros((n, n))
        for c in self.connections:
            L = Local(self.vertices[c], k=1)
            Ke = np.array(L.get_K())
            K[np.ix_(c,c)]+= Ke
        return K
    
    def Mass(self):
        n=self.vertices.shape[0]
        M = np.zeros((n, n))
        for c in self.connections:
            L = Local(self.vertices[c], k=1)
            Me = np.array(L.get_M())
            M[np.ix_(c,c)]+= Me
        return M
    
    def Stiff_D(self):
        K = self.Stiff()
        # Use np.ix_ to create a reduced matrix
        return K[np.ix_(self.internal, self.internal)]
    
    def assemble_RHS(self, RHS):
        Res = np.zeros((self.vertices.shape[0],1))
        for c in self.connections:
            L = Local(self.vertices[c], k=1)
            Res[np.ix_(c)] += L.load(RHS[np.ix_(c)]).reshape(len(c),1)  # Assuming Local.area computes the area of element c
            
        return Res
    
    def assemble_RHS1(self, RHS):
        Res = np.zeros((self.vertices.shape[0],1))
        for c in self.connections:
            L = Local(self.vertices[c], k=1)
            Res[np.ix_(c)] += L.load1(RHS[np.ix_(c)]).reshape(len(c),1)  # Assuming Local.area computes the area of element c
            
        return Res
    
    def get_RHS(self, func):
        return func(self.vertices)
    
    #Please note that this function tooks a lot of time

    def plot(self, u):
        fig, ax = plt.subplots()
        
        # Create a list of polygons and a list of colors corresponding to these polygons
        polygons = []
        color_values = []
        for polygon_indices in self.connections:
            polygon_vertices = self.vertices[polygon_indices]
            polygons.append(polygon_vertices)
            # For the color, we take the average of the values of u for the vertices of the polygon
            color_values.append(np.mean(u[polygon_indices]))

        # Create a PolyCollection
        poly_collection = PolyCollection(polygons, array=color_values, cmap='viridis', edgecolors='k')

        # Add the PolyCollection to the axis
        ax.add_collection(poly_collection)
        
        # Add a colorbar to show the mapping of u's values to colors
        fig.colorbar(poly_collection, ax=ax, label='Polygon value')

        ax.set_aspect('equal')
        plt.show()

    def L2error(self, u, ue):
        return np.sqrt(np.sum((u - ue).T @ self.M @ (u - ue)))

    def write_vtk(self, u, filename):
        with open(filename, 'w') as vtkfile:
            # Write VTK header and dataset type
            vtkfile.write("# vtk DataFile Version 3.0\n")
            vtkfile.write("Mesh data\n")
            vtkfile.write("ASCII\n")
            vtkfile.write("DATASET UNSTRUCTURED_GRID\n")

            # Write points
            vtkfile.write("POINTS {} float\n".format(len(self.vertices)))
            for vertex in self.vertices:
                vtkfile.write("{} {} {}\n".format(vertex[0], vertex[1], 0.0))

            # Count total number of vertex indices in connections
            total_vertex_indices = sum(len(face) for face in self.connections)
            vtkfile.write("CELLS {} {}\n".format(len(self.connections), total_vertex_indices + len(self.connections)))
            for face in self.connections:
                vtkfile.write("{} {}\n".format(len(face), " ".join(map(str, face))))

            # Write cell types (assuming all polygons are triangles for simplicity)
            vtkfile.write("CELL_TYPES {}\n".format(len(self.connections)))
            for _ in self.connections:
                vtkfile.write("5\n")  # VTK_TRIANGLE, change accordingly if not all faces are triangles

            # Write point data
            vtkfile.write("POINT_DATA {}\n".format(len(u)))
            vtkfile.write("SCALARS values float 1\n")
            vtkfile.write("LOOKUP_TABLE default\n")
            for value in u:
                vtkfile.write("{}\n".format(value))


# Example of how to use the class
# Ensure you have 'vertices.txt' and 'connections.txt' files with appropriate data:
