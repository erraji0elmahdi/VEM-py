import numpy as np
from geometry import normal, centroid
import scipy.linalg as la
from integrator import getH, Enumeration
from copy import copy
from quadrature import quadrature
from quad.hmatrix import h_hmatrix

P = np.array([[0., 0.], [1., 0.], [1., 1.], [0., 1.]])

P2 = np.array([[0.,0.], [3.,0.], [3.,2.], [3./2,4.], [0.,4.]])

def integrator(i,j):
    pass

class m:
    def __init__(self, centroid, h, alpha):
        self.centroid = centroid
        self.alpha = alpha
        self.ij = []
        self.h = h
        self.e = Enumeration(alpha)
        for i in range(alpha+1):
            for j in range(alpha + 1 - i):
                self.ij.append([j, i])
        
        def __call__(self, x, i):
            return np.prod([((x[0] - self.centroid[0])/self.h)**self.ij[i][0] * ((x[1] - self.centroid[1])/self.h)**self.ij[i][1] for i in range(self.alpha + 1)])
        
class grad_m:
    def __init__(self, centroid, h, alpha=1):
        self.centroid = centroid
        self.alpha = alpha
        self.ij = []
        self.h = h
        self.e = Enumeration(alpha)
        for i in range(alpha+1):
            for j in range(alpha + 1 - i):
                self.ij.append([j, i])

    def __call__(self, x, idx):
        if idx >= len(self.ij):
            raise IndexError(f"Error: idx is out of range, idx={idx}, max index={len(self.ij)-1}")
        
        # Calculate the partial derivative with respect to x
        i, j = self.ij[idx]
        if i >= 1:
            Gx = i * ((x[0] - self.centroid[0]) / self.h) ** (i - 1) * ((x[1] - self.centroid[1]) / self.h) ** j / self.h
        else:
            Gx = 0
        
        # Calculate the partial derivative with respect to y
        if j >= 1:
            Gy = j * ((x[0] - self.centroid[0]) / self.h) ** i * ((x[1] - self.centroid[1]) / self.h) ** (j - 1) / self.h
        else:
            Gy = 0

        return np.array([Gx, Gy])

class Local:
    def __init__(self, vertices, k=1):
        n = len(vertices)
        self.normals = normal(vertices)
        self.k = k
        self.num = (k + 1) * (k + 2) // 2
        self.ij = []
        for i in range(k+1):
            for j in range(k + 1 - i):
                self.ij.append([j, i])
        self.vertices = np.array(vertices)
        # Diameter (h) calculation
        self.h = np.max([np.linalg.norm(a - b) for a in self.vertices for b in self.vertices])
        # Center calculation: mean of x and y coordinates separately
        self.center = centroid(vertices)
        # Monomial lambda function adjusted with proper parenthesis for division
        self.monomial = lambda i: ((self.vertices[:, 0] - self.center[0])/ self.h)**self.ij[i][0] * ((self.vertices[:, 1] - self.center[1])/ self.h)**self.ij[i][1]
        
        sum1 = sum(vertices[i, 0] * vertices[(i + 1) % n, 1] for i in range(n))
        sum2 = sum(vertices[i, 1] * vertices[(i + 1) % n, 0] for i in range(n))
        self.area = 0.5 * abs(sum1 - sum2)
        self.e = Enumeration(k)

        self.q = quadrature(self.vertices,6)

        
        
    def get_G(self):
        G = np.zeros((self.num, self.num))
        
        for i in range(self.num):
            G[0,i]=np.mean(self.monomial(i))
        
        G[1,1]=self.area/self.h**2
        G[2,2]=self.area/self.h**2
            
        return G
    
    def get_D(self):
        D = np.zeros((len(self.vertices),self.num))
        for i in range(self.num):
            D[:,i] = self.monomial(i)
        return D
    
    def get_B(self):
        # in the matrix B, I should take into account the neumann boundary conditions
        nv=len(self.vertices)
        nm=self.num
        B = np.zeros((nm, nv))
        GM = grad_m(self.center, self.h, self.k)
        for j in range(nm):
            for i in range(nv):
                if j == 0:
                    B[j,i]= 1 / nv
                else :
                    d1 = np.linalg.norm(self.vertices[i-1] - self.vertices[i])
                    d2 = np.linalg.norm(self.vertices[i] - self.vertices[(i+1)%nv])
                    grad1 = GM(self.vertices[i-1], j)
                    grad2 = GM(self.vertices[i], j)
                    prod1 = np.dot(grad1, self.normals[i-1])
                    prod2 = np.dot(grad2, self.normals[i])
                    B[j,i]=prod1*d1/2 + prod2*d2/2
        return B
    
    def get_H(self):
        H = h_hmatrix(self.vertices, self.h, self.center)
        return H
        n = len(self.e.ij)
        H = np.zeros((n,n))
        #H[0,0]=self.area
        for i in range(0,n):
            for j in range(0,n):
                x_G = self.center[0]
                y_G = self.center[1]
                i1 = self.e(i)[0]
                j1 = self.e(i)[1]
                i2 = self.e(j)[0]
                j2 = self.e(j)[1]
                f = lambda x, y :(((x - x_G)/self.h)**i1 * ((y - y_G)/self.h)**j1)
                g = lambda x, y :(((x - x_G)/self.h)**i2 * ((y - y_G)/self.h)**j2)
                fg = lambda x, y : f(x,y) * g(x,y)
                H[i,j] = self.q(fg)
        return H
        #return getH(self.vertices, self.center[0], self.center[1], self.h, len(self.ij))
    
    def get_C(self):
        C=self.get_H()@la.inv(self.get_G())@self.get_B()
        return C
    
    def get_P0(self):
        return self.get_Pi()
    
    def get_Pi(self):
        G = self.get_G()
        B = self.get_B()
        D = self.get_D()

        Pia = la.inv(G) @ B
        Pi = D@ Pia

        return Pi
    
    def get_K(self):
        G = self.get_G()
        B = self.get_B()
        D = self.get_D()

        Pia = la.inv(G) @ B
        Pi = D@ Pia
        I = np.eye(Pi.shape[0])
        Gt = copy(G)
        Gt[0,:] = 0

        K = Pia.T@Gt@Pia + (I-Pi).T@(I-Pi)
        return K
    
    def get_M(self):
        Pi0 = self.get_P0()
        I = np.eye(Pi0.shape[0])
        H = self.get_H()
        return self.get_C().T@la.inv(H)@self.get_C() + self.area*(I-Pi0).T@(I-Pi0)
    
    def load(self, F):
        C=self.get_C()
        D=self.get_D()
        return D@C@F
    
    def load1(self, F):
        Res = np.zeros((len(self.vertices),1))
        add = self.area/len(self.vertices)
        for i in range(len(self.vertices)):
            Res[i] =  F[i] * add
        return Res